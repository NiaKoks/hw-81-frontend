import {LINKS_SUCCESS} from "../actions/actions";

const initialState ={
links:[]
};

const Reducer =(state = initialState, action) =>{
    switch (action.type) {
        case LINKS_SUCCESS:
            return{
                ...state,
                news: action.links
            };
        default:
            return state;
    }
};

export default Reducer;