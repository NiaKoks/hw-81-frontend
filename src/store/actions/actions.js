import axios from '../../axios';
export const LINKS_SUCCESS = 'LINKS_SUCCESS';
export const getLinkSuccess = (links)=>({type: LINKS_SUCCESS, links});

export const getLink = ()=>{
    return dispatch =>{return axios.get('/').then(
        response =>{dispatch(getLinkSuccess(response.data))
        }
    )}
};