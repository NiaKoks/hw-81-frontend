import React, { Component } from 'react';
import './App.css';
import {Route, Switch} from "react-router-dom";
import Main from "./components/MainPage/Main";

class App extends Component {
  render() {
    return (
        <div className="App">
          <Switch>
            <Route path="/" exact component={Main} />
            <Route path="/:shortURL" component={Main} />
          </Switch>
        </div>
    );
  }
}

export default App;