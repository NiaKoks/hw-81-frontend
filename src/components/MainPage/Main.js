import React,{Component,Fragment} from "react";
import {getLink} from "../../store/actions/actions";
import {Row,Col,Input,Button,Label} from "reactstrap"
import {connect} from "react-redux";
import "../../App.css"

class Main extends Component {
    state ={
        shortURL: '',
        fullURL: '',
        _id: this.props._id
    };
    changeHandler = (e) =>{
        this.setState({
            [e.target.name] : e.target.value
        })
    };
    sendHandler = () =>{
        this.props.getLink(this.state.fullURL)
    };

    render() {
        return (
            <Fragment>
            <Row className="ShortLinkApp">
                <Col sm={6}>
                    <Label className="Title">Short your link here:</Label>
                    <Input type="text" value={this.state.fullURL}
                           name="fullURL" id="fullURL" placeholder="Put your link here:"
                           onChange={this.changeHandler} />
                    <Button color="info" className="ShortURL" onClick={this.sendHandler}>Short NOW</Button>
                </Col>
            </Row>
            <Row>
                <Col>
                    <label className="ShortLink"> Now it's:{this.state.shortURL}</label>
                </Col>
             </Row>
            </Fragment>
        );
    }
}
const mapDispatchToProps = dispatch =>{
    return{
    getLink: (fullURL) => dispatch(getLink(fullURL))
}};
export default connect(null,mapDispatchToProps)(Main);